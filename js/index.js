/* Yes, I'm THAT lazy */
const load = $.proxy(localStorage.getItem, localStorage);
const save = $.proxy(localStorage.setItem, localStorage);

let errorHideTimeout;

$(document).ready(() => {
  /* Initialize popover for language button error */
  $("button#lang").popover({
    animation: true,
    placement: "auto",
    trigger: "manual",
    content: "Oups, impossible d'obtenir la traduction",
    title: "Il y a eu une erreur"
  });

  /* Initialize tooltips for cv and mail links */
  $("a#mail-link").tooltip({
    animation: true,
    title: "Vous pouvez me contacter par email"
  });
  $("a#cv-link").tooltip({
    animation: true,
    title: "Voici mon CV"
  });

  /* Toggle language */
  $("button#lang").click(() => changeLanguage((load("lang") === "en") ? "fr" : "en"));

  /* Default language is french */
  changeLanguage("fr");
});

/**
 * Displays error for language button for 4 seconds
 * @param {string} msg error message to display
 */
function displayError(msg) {
  clearTimeout(errorHideTimeout);
  $("button#lang").attr("data-content", msg);
  $("button#lang").popover("show");
  errorHideTimeout = setTimeout(() => $("button#lang").popover("hide"), 4000);
}

/**
 * Changes to specified language
 * @param {("en" | "fr")} lang
 */
function changeLanguage(lang) {
  save("lang", lang);
  sendAJAX();
}

/**
 * Creates an AJAX request for the language file, stores the result in words
 * @param {string} lang 
 */
function sendAJAX(lang = load("lang")) {
  $.ajax({
    url: `res/${lang}.json`,
    dataType: 'json',
    async: true,
    success: updateWords,
    error: xhr => displayError(`${xhr.status} : ${xhr.statusText}`)
  });
}

/**
 * Updates all html elements with selected language
 */
function updateWords(words) {
  $("#lang").text(words.lang);
  $("#lang").attr("data-title", words.error);
  $("#alt").attr("alt", words.profile);
  $("#heading").text(words.welcome);
  $("#short-1").text(words.nerd);
  $("#short-2").text(words.puns);
  $("#short-3").text(words.people);
  $("#presentation-name").text(words.name);
  $("#presentation-occupation").text(words.occupation);
  $("#presentation-closing").text(words.challenge);
  $("#cv-link").attr("href", words.cvFile);
  $("#cv-link").attr("data-original-title", words.cvTooltip);
  $("#mail-link").attr("data-original-title", words.mailTooltip);
}